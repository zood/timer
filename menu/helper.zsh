#!/usr/bin/zsh

typeset -A table
typeset -a names

while read name timespec; do
	table[$name]="$timespec"
	names+=$name
done < "${0:h:a}/items"

function _dmenu() {
	dmenu -b -p "Timer" \
		-nb '#3a3a3a' \
		-nf '#87afaf' \
		-sb '#87afaf' \
		-sf '#3a3a3a' \
		-fn 'Terminus:size=13'
}

start_=( --start $0:h:h/events/start )
status_=( --status $0:h:h/events/status )
checkpoint_=( --checkpoint $0:h:h/events/checkpoint )
finish_=( --finish $0:h:h/events/finish )

choice=$(print -l $names | _dmenu)
if [[ $table[$choice] ]]; then
	timer "$table[$choice]" $start_ $status_ $checkpoint_ $finish_
else
	timer "$choice" $start_ $status_ $checkpoint_ $finish_
fi
