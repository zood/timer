use std::ffi::OsString;

#[derive(Default)]
pub struct Events {
	pub start_command: Option<OsString>,
	pub checkpoint_command: Option<OsString>,
	pub status_command: Option<OsString>,
	pub finish_command: Option<OsString>,
}

impl Events {
	pub fn run_start_command(&self, hms: &str, msg: Option<&str>) {
		if let Some(cmd) = &self.start_command {
			let mut c = std::process::Command::new(cmd);
			c.arg(hms);

			if let Some(body) = msg {
				c.arg(body);
			}
			let _ = c.spawn();
		}
	}

	pub fn run_status_command(&self, percent: u8, title: &str, body: &str) {
		if let Some(cmd) = &self.status_command {
			let mut c = std::process::Command::new(cmd);
			c.args([&percent.to_string(), title, body]);
			let _ = c.spawn();
		}
	}

	pub fn run_checkpoint_command(&self, hms: &str, msg: Option<&str>, raw: &str) {
		if let Some(cmd) = &self.checkpoint_command {
			let mut c = std::process::Command::new(cmd);
			c.args([hms, raw]);
			if let Some(msg) = msg {
				c.arg(msg);
			}
			let _ = c.spawn();
		}
	}

	pub fn run_finish_command(&self, raw: &str, hms: Option<&str>) {
		if let Some(cmd) = &self.finish_command {
			let mut c = std::process::Command::new(cmd);
			c.arg(raw);

			if let Some(hms) = hms {
				c.arg(hms);
			}
			let _ = c.spawn();
		}
	}
}
