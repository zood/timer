#![feature(slice_take)]

use std::{
	io::{self, BufRead, BufReader, Read, Write},
	os::unix::net::{UnixListener, UnixStream},
	path::PathBuf,
	sync::Arc,
	thread,
	time::Instant,
};

use fork::{daemon, Fork};
use is_executable::is_executable;

use crate::time::Timer;

mod cli;
mod events;
use cli::Action;
use events::Events;
mod socket;
mod time;

struct App;
impl App {
	const HELP: &str = "
   Usage:
      timer ACTION
   ACTIONS:
      status - report status for all the running timers
      undo   - undo the last started timer
   STARTING A NEW TIMER;
      #h#m#s[{MSG}][-#h#m#s[{MSG}]][=#h#m#s[{MSG}]] | H:M[{MSG}]
   NEW TIMER OPTIONS:
      --finish CMD [--start CMD] [--status CMD] [--checkpoint CMD]";
	fn die(self) -> ! { std::process::exit(1); }
	fn warn(self, msg: impl std::fmt::Display) {
		eprintln!("[timer] WARNING: {msg}");
	}
	fn err(self, msg: impl std::fmt::Display) -> Self {
		eprintln!("[timer] ERROR: {msg}");
		self
	}
	fn help(self) -> Self {
		eprintln!("{}", Self::HELP);
		self
	}
}

fn run_new_timer(
	start: Instant,
	raw_timepoint: Arc<String>,
	timer: Timer,
	sock: Arc<PathBuf>,
	checkps: Arc<Vec<Timer>>,
	events: Arc<Events>,
) -> io::Result<()> {
	if let Ok(Fork::Child) = daemon(false, true) {
		let listener = UnixListener::bind(&*sock)?;
		let sock_ = sock.clone();
		let main_dur = timer.dur;
		let main_secs = timer.as_secs();
		let main_msg = timer.msg;
		let main_msg_ = main_msg.clone();
		let raw_timepoint_ = raw_timepoint.clone();
		let checkps_ = checkps.clone();
		let events_ = events.clone();

		events.run_start_command(&time::secs2hms(main_secs), main_msg.as_deref());
		thread::spawn(move || {
			for mut stream in listener.incoming().flatten() {
				let reader = BufReader::new(&stream);
				for line in reader.lines().map_while(Result::ok) {
					match line.as_ref() {
						"terminate" => {
							let timeout_left =
								time::secs2hms((main_dur - start.elapsed()).as_secs());
							write!(&mut stream, "{} -> {}", raw_timepoint_, timeout_left).unwrap();
							std::fs::remove_file(&*sock_).unwrap();
							std::process::exit(0);
						}
						"status" => {
							let mut marks = Vec::new();
							checkps_
								.iter()
								.filter(|t| t.dur > start.elapsed())
								.map(|t| (t.dur - start.elapsed()).as_secs())
								.for_each(|t| marks.push(t));
							let timeout_left = (main_dur - start.elapsed()).as_secs();
							marks.push(timeout_left);
							marks.sort();
							let mut marks: Vec<String> =
								marks.into_iter().map(time::secs2hms).collect();
							marks.push(main_msg_.as_ref().unwrap_or(&raw_timepoint_).to_string());
							let (title, rest) = marks.split_first().unwrap();
							let body = rest.join("\n");
							let progress = ((start.elapsed().as_secs() as f64 / main_secs as f64)
								* 100.0) as u8;
							events_.run_status_command(progress, title, &body)
						}
						_ => (),
					}
				}
			}
		});

		thread::scope(|s| {
			for ti in &*checkps {
				s.spawn(|| {
					thread::sleep(ti.dur - start.elapsed());
					events.run_checkpoint_command(
						&time::secs2hms(ti.as_secs()),
						ti.msg.as_deref(),
						main_msg.as_ref().unwrap_or(&raw_timepoint),
					);
				});
			}
		});
		thread::sleep(main_dur - start.elapsed());
		events.run_finish_command(&raw_timepoint, main_msg.as_deref());
		std::fs::remove_file(&*sock)?;
	}
	Ok(())
}

fn timers_status() -> io::Result<()> {
	for sock in socket::files()? {
		let mut stream = UnixStream::connect(sock)?;
		writeln!(&mut stream, "status")?;
	}
	Ok(())
}

fn undo_last_timer() -> io::Result<()> {
	if let Some(sock) = socket::recent() {
		let mut stream = UnixStream::connect(sock)?;
		writeln!(&mut stream, "terminate")?;
		let mut response = String::new();
		stream.read_to_string(&mut response)?;
		println!("{response}");
	}
	Ok(())
}

macro_rules! check_exec {
	($command:expr, $not_provided:expr, $not_exec:expr) => {
		match $command {
			None => $not_provided,
			Some(ref cmd) =>
				if !is_executable(cmd) {
					$not_exec;
				},
		}
	};
}

fn main() {
	let args = match cli::parse_args() {
		Ok(args) => args,
		Err(e) => App.err(e).help().die(),
	};

	match args.action {
		Some(action) => match action {
			Action::ReportStatus => timers_status().unwrap_or_else(|e| App.err(e).die()),
			Action::UndoLastTimer => undo_last_timer().unwrap_or_else(|e| App.err(e).die()),
			Action::SetTimer(timer, checkpoints, raw_timepoint) => {
				check_exec!(
					args.events.finish_command,
					App.err("no --finish CMD provided").die(),
					App.err("--finish CMD is not an executable").die()
				);
				check_exec!(
					args.events.status_command,
					App.warn("no --status CMD provided"),
					App.err("--status CMD is not an executable").die()
				);
				if !checkpoints.is_empty() {
					check_exec!(
						args.events.checkpoint_command,
						App.warn("no --checkpoint CMD provided"),
						App.err("--checkpoint CMD is not an executable").die()
					);
				}
				run_new_timer(
					Instant::now(),
					Arc::new(raw_timepoint),
					timer,
					Arc::new(socket::new().unwrap_or_else(|e| App.err(e).die())),
					Arc::new(checkpoints),
					Arc::new(args.events),
				)
				.unwrap_or_else(|e| App.err(e).die());
			}
		},
		None => App.help().die(),
	}
}
