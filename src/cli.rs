use crate::{events::Events, time::Timer};

pub enum Action {
	ReportStatus,
	UndoLastTimer,
	SetTimer(Timer, Vec<Timer>, String),
}

pub struct Args {
	pub action: Option<Action>,
	pub events: Events,
}

pub fn parse_args() -> Result<Args, lexopt::Error> {
	use lexopt::prelude::*;
	let mut parser = lexopt::Parser::from_env();
	let mut events = Events::default();
	let mut action: Option<Action> = None;
	while let Some(arg) = parser.next()? {
		match arg {
			Long("start") => events.start_command = Some(parser.value()?),
			Long("checkpoint") => events.checkpoint_command = Some(parser.value()?),
			Long("status") => events.status_command = Some(parser.value()?),
			Long("finish") => events.finish_command = Some(parser.value()?),
			Value(val) if action.is_none() => match val.to_string_lossy().as_ref() {
				"status" => action = Some(Action::ReportStatus),
				"undo" => action = Some(Action::UndoLastTimer),
				other => match crate::time::parse::timespec(other) {
					Ok((timer, checkpoints)) => {
						let raw_timepoint = crate::time::parse::raw_timepoint(other).unwrap();
						action = Some(Action::SetTimer(timer, checkpoints, raw_timepoint))
					}
					Err(_) => return Err("invalid timespec".into()),
				},
			},
			_ => return Err(arg.unexpected()),
		}
	}
	Ok(Args { action, events })
}
